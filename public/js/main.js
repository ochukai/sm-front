+function ($) {

    $(function() {

        /*
         * 频率控制 返回函数连续调用时，fn 执行频率限定为每多少时间执行一次
         */
        var throttle = function (fn, delay, immediate, debounce) {
            var curr = +new Date(),//当前时间
                last_call = 0,
                last_exec = 0,
                timer = null,
                diff, //时间差
                context,//上下文
                args,
                exec = function () {
                    last_exec = curr;
                    fn.apply(context, args);
                };

            return function () {
                curr= +new Date();
                context = this,
                    args = arguments,
                    diff = curr - (debounce ? last_call : last_exec) - delay;
                clearTimeout(timer);

                if (debounce) {
                    if (immediate) {
                        timer = setTimeout(exec, delay);
                    } else if (diff >= 0) {
                        exec();
                    }
                } else {
                    if (diff >= 0) {
                        exec();
                    } else if (immediate) {
                        timer = setTimeout(exec, -diff);
                    }
                }

                last_call = curr;
            }
        };

        var onResize = function() {
            console.log(
                'window resize width:', $(document).width(),
                'height:', $(document).height(),
                'body height:', $(document.body).height(),
                'outheight:',   $(document.body).outerHeight(true),
                'window width:', $(window).width(),
                'height:', $(window).height()
            );

            var currentBodyHeight = $(document.body).outerHeight(true);
            var windowHeight = $(window).height();

            if ($('.footer').hasClass('navbar-fixed-bottom')
                && windowHeight < (currentBodyHeight + $('.footer').outerHeight(true))) {
                $('.footer').removeClass('navbar-fixed-bottom');
                return;
            } else if (windowHeight > currentBodyHeight) {
                $('.footer').addClass('navbar-fixed-bottom');
            }
        };

        // run when current window is loaded.
        onResize();

        // resize 时间执行太频繁 使用throttle 的方式会限制它的执行速度
        // 当窗口被改变大小时，我这里设置的是250毫秒执行一次resize事件
        $(window).on('resize', throttle(onResize, 250));

        // auto hidden navigation
        //var myElement = document.querySelector("div.navbar-fixed-top.navigation");
        //var headroom  = new Headroom(myElement, {
        //    "tolerance": 5,
        //    "offset": 200,
        //    "classes": {
        //        "initial": "animated",
        //        "pinned": "slideDown",
        //        "unpinned": "slideUp"
        //    }
        //});

        // headroom.init();

        $('.modal').on('show.bs.modal', function (event) {
            var nav  = $('div.navbar-fixed-top.navigation .container');
            var left = nav.offset().left;
            nav.css({
                position: 'absolute',
                left: left
            });
        });

        $('.modal').on('hidden.bs.modal', function (event) {
            $('div.navbar-fixed-top.navigation .container').css({ position: 'none' });
        });

        // in index
        $('#btn-login').on('click', function(event) {
            $('#modal-login').modal('show');
            event.preventDefault();
        });

        $('#btn-register').on('click', function(event) {
            $('#modal-register').modal('show');
            event.preventDefault();
        });

        // in login dialog
        $('#btn-forget').on('click', function() {
            $('#modal-login').modal('hide');
            setTimeout(function() {
                $('#modal-forget').modal('show');
            }, 500);
        });

        $('#btn-log-register').on('click', function() {
            $('#modal-login').modal('hide');
            setTimeout(function() {
                $('#modal-register').modal('show');
            }, 500);
        });

        // in forget
        $('#btn-forget-login').on('click', function() {
            $('#modal-forget').modal('hide');
            setTimeout(function() {
                $('#modal-login').modal('show');
            }, 500);
        });

        // in register
        $('#btn-reg-login').on('click', function() {
            $('#modal-register').modal('hide');
            setTimeout(function() {
                $('#modal-login').modal('show');
            }, 500);
        });

        // scroll to top
        $(window).scroll(function () {
            if ($(this).scrollTop() > 200) {
                $('.scroll-to-top').show();
            } else {
                $('.scroll-to-top').fadeOut();
            }
        });

        $('.scroll-to-top').on('click', function () {
            $("html, body").animate({ scrollTop: 0 }, 500);
            return false;
        });

        // to-pay
        $('#btn-go-to-pay').on('click', function() {
            $('#modal-pay').modal({ backdrop: 'static' }).modal('show');
            // $('#payment_form').submit();
        });

        // company-info
        $('#btn-fill-compnay-info').on('click', function() {
            $('#modal-company-info').modal({ backdrop: 'static' }).modal('show');
        });

        // company-info province-city-combo-select
        $('#re-address').citySelect({ prov:"上海", nodata:"none" });
        /*add event here*/
        $('#re-address select').on('change', function() {
            setTimeout(function() {
                var prefix = $('#re-address select').toArray().map(function(item) {
                    return $(item).val();
                });
                $('#label-address-prefix').text(prefix.join('-'));
            }, 300);
        });

        // override jquery validate plugin defaults
        $.extend($.validator.messages, {
            required: "请填写该字段",
            remote: "请修正该字段",
            email: "请输入正确格式的电子邮件",
            url: "请输入合法的网址",
            date: "请输入合法的日期",
            dateISO: "请输入合法的日期 (ISO).",
            number: "请输入合法的数字",
            digits: "只能输入整数",
            creditcard: "请输入合法的信用卡号",
            equalTo: "请再次输入相同的值",
            accept: "请输入拥有合法后缀名的字符串",
            maxlength: jQuery.validator.format("请输入一个 长度最多是 {0} 的字符串"),
            minlength: jQuery.validator.format("请输入一个 长度最少是 {0} 的字符串"),
            rangelength: jQuery.validator.format("请输入 一个长度介于 {0} 和 {1} 之间的字符串"),
            range: jQuery.validator.format("请输入一个介于 {0} 和 {1} 之间的值"),
            max: jQuery.validator.format("请输入一个最大为{0} 的值"),
            min: jQuery.validator.format("请输入一个最小为{0} 的值")
        });


        $.validator.setDefaults({
            highlight: function(element) {
                $(element).closest('.form-group').addClass('has-error').removeClass('has-success');
            },
            unhighlight: function(element) {
                $(element).closest('.form-group').addClass('has-success').removeClass('has-error');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function(error, element) {
                if(element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            }
        });

        $.validator.addMethod(
            "isPhone",
            function (value, element) {
                var mobile = /^(((13[0-9]{1})|(15[0-9]{1}))+\d{8})$/;
                var tel = /^\d{3,4}-?\d{7,9}$/;
                return this.optional(element) || (tel.test(value) || mobile.test(value));
            },
            "请正确填写您的联系电话");

        $("#form-company-info").validate({
            rules: {
                'name': {
                    required: true
                },
                'taxCode': {
                    required: true
                },
                'manager': {
                    required: true
                },
                'phone': {
                    required: true,
                    isPhone: true
                },
                'receiverAddress': {
                    required: true
                },
                'receiver': {
                    required: true
                },
                'receiverPhone': {
                    required: true
                }
            }

            // add message later
            //, messages: {
            //    firstname: "请输入姓名",
            //    email: {
            //        required: "请输入Email地址",
            //        email: "请输入正确的email地址"
            //    },
            //    password: {
            //        required: "请输入密码",
            //        minlength: jQuery.format("密码不能小于{0}个字 符")
            //    },
            //    confirm_password: {
            //        required: "请输入确认密码",
            //        minlength: "确认密码不能小于5个字符",
            //        equalTo: "两次输入密码不一致不一致"
            //    }
            //}
        });

        $('#btn-company-info-submit').on('click', function() {
            $('#form-company-info').submit();
        });

    });

}(window.jQuery);
